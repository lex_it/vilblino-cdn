/* 	==|== plugins.js =====================================================
	Author: James Borillo
	Company: Lex Norsk Samfunnsinformasjon as
	Department: IT
	========================================================================== */

/* 
	Usage: log('inside coolFunc', this, arguments);
*/
window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console) {
    arguments.callee = arguments.callee.caller;
    var newarr = [].slice.call(arguments);
    (typeof console.log === 'object' ? log.apply.call(console.log, console, newarr) : console.log.apply(console, newarr));
  }
};

/*
	Description: Extends the Browser object and check if the browser is mobile
	@returns BOOLEAN 
*/
$.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

/* 
	Make it safe to use console.log always
*/
(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();){b[a]=b[a]||c}})((function(){try
{console.log();return window.console;}catch(err){return window.console={};}})());

/* 
	Get all URL parameters 
	Ex. var allVars = $.getUrlVars(); OR var byName = $.getUrlVar('name');
*/
$.extend({
	getUrlVars: function(){
    	var vars = [], hash;
    	var url = window.location.href.replace(/&amp;/gi, '&');
    	var hashes = url.slice(url.indexOf('?') + 1).split('&');
    
    	if (hashes[0] != url) { // if no parameter... (hack: array 0 will take the url if no parameter in the url)
    		for(var i = 0; i < hashes.length; i++) {
				hash = hashes[i].split('=');
    			vars.push(hash[0]);
    			vars[hash[0]] = hash[1];
    		}
    	}
    	return vars;
  	},
	getUrlVar: function(name){
		return $.getUrlVars()[name];
  	}
});

/*
	Get parameter by name
*/
function getParameterByName(name, url) { // name: required, {url:optional} - if no second parameter, method will force to get the url from the browser
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	if(length.url<1) {
		url = window.location.href;
	}
	var results = regex.exec(url);
	if(results == null){
		return "";
	} else {
		return decodeURIComponent(results[1].replace(/\+/g, " "));
	}
}

/*
	Google Maps (Lex' plugin) 
*/
function initialize(map_element_id, locations, show_map_with, map_type_control, street_view_control, overview_map_control, sidebar_title ) {
	
	var map = new google.maps.Map(document.getElementById(map_element_id), {
		mapTypeControl: map_type_control,
		streetViewControl: street_view_control,
		overviewMapControl: overview_map_control,
		panControl: false, 
		zoomControl: true, // Recommended that this is always true
		scaleControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		navigationControlOptions: {
  			style: google.maps.NavigationControlStyle.SMALL
   		},
   		mapTypeControlOptions: {
   			style: google.maps.MapTypeControlStyle.DEFAULT
   		},
   		zoom: 15, // For single marker (Will be overriden if more than 1 marker)
   		center:  new google.maps.LatLng(locations[0][0], locations[0][1]) // For single marker (Will be overriden if more than 1 marker)	
	});
	
	if (locations.length > 1) { //Override the default zoom and center: Auto zoom and center map canvas if markers are more than 1
		bounds = new google.maps.LatLngBounds();
		
		for(var i in locations) {
			var with_marker = (locations[i][0]!=0) & (locations[i][1]!=0);
   			if ( with_marker ){
				var ll = new google.maps.LatLng(locations[i][0], locations[i][1]);
   				bounds.extend(ll);
			}
		}
		map.fitBounds(bounds);
	}
	
	if ( (show_map_with == "DrivingDirection") && (locations.length == 1)  ) {
		  		
		if (navigator.geolocation) {
			function getDrivingDirection(my_lat, my_lon) {
				if ( jQuery('#map_sidebar').length < 1 ) { // initial position
					jQuery('#'+map_element_id).parent().prepend('<div id="map_sidebar"></div>');
				}
				var directionDisplay;
				var directionsService = new google.maps.DirectionsService();
				directionsDisplay = new google.maps.DirectionsRenderer();
				directionsDisplay.setMap(map);
				directionsDisplay.setPanel(document.getElementById("map_sidebar"));
					
				var start = my_lat+", "+my_lon; //Exact geolocation
				var end = locations[0][0]+", "+locations[0][1];
				
				var request = {
					origin:start, 
					destination:end,
					travelMode: google.maps.DirectionsTravelMode.DRIVING
				};
				
				directionsService.route(request, function(response, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(response);
					}
				});
			}
			
			navigator.geolocation.getCurrentPosition(    		
				function (position){
					getDrivingDirection(position.coords.latitude, position.coords.longitude);
					console.log( "Initial Position Found" );
				},
				function (error) {
					console.log( "Something went wrong: ", error );
				}
				,
				{
					timeout: (5 * 1000),
					maximumAge: (1000 * 60 * 1), //1 minute
					enableHighAccuracy: true
				}
			);
			
			/*var positionTimer = navigator.geolocation.watchPosition(
				function( position ){
					getDrivingDirection(position.coords.latitude, position.coords.longitude, true);
					console.log( "Newer Position Found" );
				}
			);
			setTimeout(
				function(){
					// Clear the position watcher.
					navigator.geolocation.clearWatch( positionTimer );
				},
				(1000 * 60 * 5)
			);*/
		} else {
			console.log( "Something went wrong: ", error );
			alert("Impossible to track your location in order to generate driving direction. Normal map view will now be generated.");
			setMarkers(map, locations, map_element_id, 'SidebarLinks', sidebar_title); // Fallback: Force to show the map with a marker
		}
    	
    } else {
    	setMarkers(map, locations, map_element_id, show_map_with, sidebar_title);
	}
	
} // intialize();

function setMarkers(map, locations, map_element_id, show_map_with, sidebar_title) {
	markers = new Array();
	var shadow = new google.maps.MarkerImage('http://google-maps-icons.googlecode.com/files/shadow.png',
    	new google.maps.Size(51, 37),
    	new google.maps.Point(0, 0),
    	new google.maps.Point(10, 28)
    );
  	
  	//Ignore this group if without external links
  	var externalLinks = '';
  	var countyChange = false;
  	
  	for (var i = 0; i < locations.length; i++) {
    	
		var with_marker = (locations[i][0]!=0) & (locations[i][1]!=0);
		var myLatLng = new google.maps.LatLng(locations[i][0], locations[i][1]);
		var nr = i + 1;
		if ( locations.length < 2 ) {
			var image = web_static_content_path+'/gmarker/red_marker.png';
		} else if ( locations.length < 100 ) {
			if(web_static_content_path.indexOf('http://') < 0) {
				var image = web_static_content_path+'/gmarker_/yellow_marker.png?text=' + nr;
			}
			else {
    			var image = web_static_content_path+'/gmarker/google_marker.php?text=' + nr + '&marker=yellow_marker';
    		}
    	} else {
    		var image = web_static_content_path+'/gmarker/yellow_marker.png';
    	}
    	marker = new google.maps.Marker({
       		position: myLatLng,
       		map: map,
       		shadow: shadow,
       		icon: image,
       		title: nr.toString(), //locations[i][3],
       		zIndex: locations.length - nr
    	});
   	    
		markers.push(marker);
	
    	google.maps.event.addListener(marker, 'mouseover', (function() {
    		origZIndex = this.getZIndex();
			this.setZIndex( 1000 );
          	if ( locations.length < 2 ) {
				var image = web_static_content_path+'/gmarker/red_marker.png';
			} else if ( locations.length < 100 ) {
				if(web_static_content_path.indexOf('http://') < 0) {
					this.setIcon(web_static_content_path+'/gmarker_/orange_marker.png?text=' + this.getTitle());
				}
				else {
	    			this.setIcon(web_static_content_path+'/gmarker/google_marker.php?text=' + this.getTitle() + '&marker=orange_marker');
				}
    		} else {
    			this.setIcon(web_static_content_path+'/gmarker/yellow_marker.png');
    		}

        }));
       
        google.maps.event.addListener(marker, 'mouseout', (function() {
          	this.setZIndex( origZIndex );
          	if ( locations.length < 2 ) {
				var image = web_static_content_path+'/gmarker/red_marker.png';
			} else if ( locations.length < 100 ) {
				if(web_static_content_path.indexOf('http://') < 0) {
					this.setIcon(web_static_content_path+'/gmarker_/yellow_marker.png?text=' + this.getTitle());
				}
				else {
	    			this.setIcon(web_static_content_path+'/gmarker/google_marker.php?text=' + this.getTitle() + '&marker=yellow_marker');
				}
    		} else {
    			this.setIcon(web_static_content_path+'/gmarker/yellow_marker.png');
    		}
        }));
       
        if ( show_map_with == "SidebarLinks" ) {
			infowindow = new google.maps.InfoWindow();
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(
						'<a href="/'+locations[i][locations[i].length-1]+locations[i][4]+'"><strong>' + locations[i][3] + '</strong></a><br>' +
						'<em style="color:#888;">Adresse</em> ' + locations[i][5] + '<br>' +
						'<em style="color:#888;">Telefon</em> ' + locations[i][6] + '<br>' +
						'<em style="color:#888;">E-post</em> <a href="mailto:'+locations[i][7]+'">' + locations[i][7] + '</a>'
					);
					infowindow.open(map, marker);
					map.setCenter(new google.maps.LatLng(locations[i][0], locations[i][1]));	
					map.setZoom(15);
				}
			})(marker, i));
			
			//Concatinate contents for the map_sidebar
			countyChange = false;
			if(i == 0) {
				externalLinks += '<li><strong>'+locations[i][8]+'</strong></li>';
				countyChange = true;
			}
			else if(locations[i][8] != locations[i-1][8]) {
				externalLinks += '<li><strong>'+locations[i][8]+'</strong></li>';
				countyChange = true;
			}
			
			if (countyChange) {
				externalLinks += '<li>'+locations[i][2]+'</li>';
			}
			else if (locations[i][2] != locations[i-1][2]) {
				externalLinks += '<li>'+locations[i][2]+'</li>';
			}
																	
			if ( with_marker ) {
				externalLinks += '<li><a href="javascript:externalLinkEvent('+i+', \'click\');" onmouseover="javascript:externalLinkEvent('+ i +', \'mouseover\');" onmouseout="javascript:externalLinkEvent('+i+', \'mouseout\');">' + nr + ' ' + locations[i][3] + '</a></li>';
			} else {
				externalLinks += '<span style="color:#AAA">'+ nr + ' ' + locations[i][3] + '</span><br>';
			}
		}

  	} // end for
  	
  	if ( show_map_with == "SidebarLinks" ) {
  		externalLinks = '<div id="map_sidebar"><h2>'+sidebar_title+'</h2><ul>' + externalLinks + '</ul></div>';
  		jQuery('#'+map_element_id).parent().prepend(externalLinks);
  	} 
  		
} // setMarkers()

function externalLinkEvent(i, myEvent, with_marker) {
	google.maps.event.trigger(markers[i], myEvent);
} //externalLinkEvent()

/*
	Fylkeskart (Vilblino's plugin)
*/
function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		norge_akershus_over = newImage("../v1/Fylke/norge-akershus_over.gif");
		norge_nordtrondelag_over = newImage("../v1/Fylke/norge-nordtrondelag_over.gif");
		norge_oppland_over = newImage("../v1/Fylke/norge-oppland_over.gif");
		norge_sortrondelag_over = newImage("../v1/Fylke/norge-sortrondelag_over.gif");
		norge_moreogromsdal_over = newImage("../v1/Fylke/norge-moreogromsdal_over.gif");
		norge_sognogfjordane_over = newImage("../v1/Fylke/norge-sognogfjordane_over.gif");
		norge_oslo_over = newImage("../v1/Fylke/norge-oslo_over.gif");
		norge_hedmark_over = newImage("../v1/Fylke/norge-hedmark_over.gif");
		norge_hordaland_over = newImage("../v1/Fylke/norge-hordaland_over.gif");
		norge_rogaland_over = newImage("../v1/Fylke/norge-rogaland_over.gif");
		norge_finnmark_over = newImage("../v1/Fylke/norge-finnmark_over.gif");
		norge_vestagder_over = newImage("../v1/Fylke/norge-vestagder_over.gif");
		norge_troms_over = newImage("../v1/Fylke/norge-troms_over.gif");
		norge_austagder_over = newImage("../v1/Fylke/norge-austagder_over.gif");
		norge_telemark_over = newImage("../v1/Fylke/norge-telemark_over.gif");
		norge_buskerud_over = newImage("../v1/Fylke/norge-buskerud_over.gif");
		norge_vestfold_over = newImage("../v1/Fylke/norge-vestfold_over.gif");
		norge_nordland_over = newImage("../v1/Fylke/norge-nordland_over.gif");
		norge_ostfold_over = newImage("../v1/Fylke/norge-ostfold_over.gif");
		preloadFlag = true;
	}
}

function flyttkart(tmp){
	if (navigator.appName == "Microsoft Internet Explorer" && navigator.userAgent.indexOf("Mac") > 0) {
		//alert ("You are using a Mac.")
		document.getElementById(tmp).style.left = '17em';
		document.getElementById(tmp).style.top = '11em';
	}
}

/*
	Cache images 
*/
(function($) {
  var cache = [];
  // Arguments are image paths relative to the current page.
  $.preLoadImages = function() {
    var args_len = arguments.length;
    for (var i = args_len; i--;) {
      var cacheImage = document.createElement('img');
      cacheImage.src = arguments[i];
      cache.push(cacheImage);
    }
  }
})(jQuery)

function replaceQueryParameter(qs, name, value) {
	var newParam = name + '=' + value;
	if (qs.indexOf(name + '=') == -1) {
		if (qs == '') {
			qs = '?'
		}
		else {
			qs = qs + '&'
		}
		qs = qs + newParam;
	}
	else {
		var start = qs.indexOf(name + "=");
		var end = qs.indexOf("&", start);
		if (end == -1) {
			end = qs.length;
		}
		var curParam = qs.substring(start, end);
		qs = qs.replace(curParam, newParam);
	}
	return qs;
}
