﻿$(document).ready(function () {
	setRelateActions();
	setActions();
});

if (!Array.prototype.indexOf) {
	var i = 0;
	var fromIndex = 0;
 	Array.prototype.indexOf = function (obj, fromIndex) {
    if ((fromIndex === null) || (fromIndex === undefined)) {
        fromIndex = 0;
    } else if (fromIndex < 0) {
        fromIndex = Math.max(0, this.length + fromIndex);
    }
    for (i = fromIndex; i < this.length; i++) {
        if (this[i] === obj) {
            return i;
		}
    }
    return -1;
  };
}

function eventTrigger(e) {
    if (! e) {e = event;}
	return e.target || e.srcElement;
}

var ScrollPosition = {
	x: 0,
	y: 0,
	setX: function(px) { this.x = px; },
	setY: function(py) { this.y = py; },
	getX: function() { return this.x; },
	getY: function() { return this.y; },
	clear: function() { this.x = 0; this.y = 0; }
};

function showElement(id) {
	$("#"+id).removeClass("hidden");
}

function hideElement(id) {
	$("#"+id).addClass("hidden");
}

function myCenter(element /* optional*/, width, height, minY ) {
	if(minY === undefined) { minY = 30;}
	if((!(width)) && (!(height))) {
		width = parseInt(element.css("width").replace(/px/, ""), 10);
		height = parseInt(element.css("height").replace(/px/, ""), 10);
	}
	if(width < 0) {width = -width;}
	if(height < 0) {height = -height;}
	var wndWidth, wndHeight;
	wndWidth = $(window).width();
	wndHeight = $(window).height();
	var xpos = Math.round(Math.max(0, (wndWidth/2 - width/2) + $(window).scrollLeft()));
	var ypos = Math.round(Math.max(minY, ((wndHeight - height)/3) + $(window).scrollTop()));
	element.css("left", xpos + "px");
	element.css("top", ypos + "px");
	return [xpos, ypos];
}

function setValueById(id, theValue) {
	var element = $("#"+id);
	if(element.length > 0) {
		switch (element[0].nodeName.toUpperCase()) {
			case "DIV":
				element.html(theValue);
				break;
			case "TD":
				element.html(theValue);
				break;
			default:
				element.val(theValue);
				break;
		}
	}
}

function getValueById(id) {
	var element = $("#"+id);
	if(element.length > 0) {
		switch (element[0].nodeName.toUpperCase()) {
			case "DIV":
				return element.html();
			case "TD":
				return element.html();
			default:
				return element.val();
		}
	}
}

function checkValue(e) {
	var felt = jQuery(eventTrigger(e));
	AJAX.url = "/4daction/Web_Input";
	AJAX.parameters.id = felt.attr("id");
	AJAX.parameters.value = $("#"+AJAX.parameters.id).val();
	AJAX.post();
}

function gup( name ) {
	if($("#"+name)) {
		hiddenValue = $("#"+name).val();
	}
	if(hiddenValue) {
		return hiddenValue;
	}
	var url = document.location.href.replace(/&amp;/g,"&");
	url = url.replace(/%26amp;/g,"&");
	url = url.replace(/%26/g,"&");
	url = url.replace(/%3D/g,"=");
	name = name.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( url );
	var hiddenValue;
	if( results !== null ) {
		return results[1];
	}
	return "";
}

function setUserParam(name, value) {
	var i = 0;
	var Search = document.location.search.replace(/&amp;/g,"&");
	Search = Search.replace(/%26amp;/g,"&");
	Search = Search.replace(/%26/g,"&");
	Search = Search.replace(/%3D/g,"=");
	
	var parameters = Search.split("&");
	var endret = false;
	var ii = parameters.length;
	for(i=0; i < ii; i++) {
		if(parameters[i].indexOf("?"+name)>-1) {
			parameters[i] = "?" + name +"=" + value;
			i = ii;
			endret = true;
		} else if(parameters[i].indexOf("&"+name)>-1){
			parameters[i] = "&" + name + "=" + value;
			i = ii;
			endret = true;
		}
	}
	Search = parameters.join("&amp;");
	return Search;
}

function my_getStates() {
	return "";
	//return "ASP="+gup("ASP") + "&amp;Random=" + gup("Random");
}

function regIsEmail(fData) {
	var i = 0;
	var PrefixStop = fData.indexOf("@");
	if(PrefixStop > 0) {
		fDataPrefix = fData.substring(0,PrefixStop);
		Pos = fDataPrefix.indexOf(".");
		CountDots = 0;
		while (Pos>-1) {
			CountDots++;
			Pos = fDataPrefix.indexOf(".", Pos+1);
		}
	
		PrefixPattern = "^[0-9a-zA-Z_-]+";
		for (i = 0;i < CountDots; i++) {
			PrefixPattern+="[\\.]?[0-9a-zA-Z_-]+";
		}
		
		fDataPostfix = fData.substring(PrefixStop+1);
		Pos = fDataPostfix.indexOf(".");
		CountDots = 0;
		while (Pos>-1) {
			CountDots++;
			Pos = fDataPostfix.indexOf(".", Pos+1);
		}
		
		PostfixPattern = "@[0-9a-zA-Z_-]";
		for (i = 1; i < CountDots; i++) {
			PostfixPattern+="+[\\.]?[0-9a-zA-Z_-]";
		}
		PostfixPattern+="+[\\.]{1}[0-9a-zA-Z_-]+$";
		
		var fDatapattern = PrefixPattern+PostfixPattern;
		var reg = new RegExp(fDatapattern);
		return reg.test(fData);
	}
	else {
		return false;
	}
}
			
function regIsNumber(fData) {
	var reg = new RegExp("^[0-9]+$");
	return reg.test(fData);
}

function my4daction() {
	var url = document.location.href;
	var pos = url.indexOf("/4daction/");
	if(pos > -1) {
		url = url.substring(pos + 10);
		pos = url.indexOf("/");
		if(pos > -1 ) {
		}
		else {
			pos = url.indexOf("?");
			if(pos > -1 ) {
			}
			else {
				return url;
			}
		}
		return url.substring(0, pos);
	}
	else {
		return "";
	}
}

function toggleUL(id) {
	if($("#"+id+" > li.hidden").length > 0 ) {
		$("#"+id+" > li").removeClass("hidden");
	}
	else {
		$("#"+id+" > li.ekstra").addClass("hidden");
	}
}

function showPreview(e) {
	var source = jQuery(eventTrigger(e));
	var id = source.attr("id");
	var previewId = id +"_";
	var pos = source.position();
	$("#"+previewId).css("top", pos.top + 20).css("left", pos.left + 20).fadeIn("slow");
}
function hidePreview(e) {
	var source = jQuery(eventTrigger(e));
	$("#"+source.attr("id")+"_").fadeOut("slow");
}

/****************** AJAX *******************/
var AJAX;
if (AJAX && (typeof AJAX != "object" || AJAX.NAME)) {
    throw new Error("Namespace 'AJAX' already exists");
}
AJAX = {
	url: "",
	method: "",
	parameters: {},
	extra: {},
	callback: function(response) {
		if(typeof response == "string") {
			var xml = $.createXMLDocument(response);
			if(xml) {
				response = xml;
			}
		}
		var error = $(response).find("error").val();
		if(error === undefined) {
			ajaxResponse(response);
		}
		else if(error.length>0) {
			displayError(error);
		}
		else {
			ajaxResponse(response);
		}
	},
	post: function() {
		this.parameters.ASP = gup("ASP");
		this.parameters.Random = gup("Random");
		this.parameters.ax = 1;
		$.post(this.url, this.parameters,this.callback);
	},
	get_: function() {
		$.get(this.url, this.callback);
	}
};
/*************** End AJAX *******************/
function getRedirect(theURL, parameters, title) {
	if(parameters === undefined) {parameters = "";}
	parameters = (theURL.indexOf("ASP=")>-1? parameters: my_getStates() + parameters);
	var url = theURL.charAt(0)=="/"? location.host + theURL : theURL;
	url = location.protocol + "//" + url + parameters;
	if(title) {
		//åpne nytt vindu
		window.open(url, title);
	}
	else {
		window.location = url;
	}
}
function moveRow(root, row, selector, up, circular) {
	var i = 0;
	var rows = root.find(selector);
	row.remove();
	for(i = 0; i < rows.length; i++) {
		rows[i] = jQuery(rows[i]);
		if(rows[i].attr("id") == row.attr("id")) {
			if(up) {
				if(i !== 0) {
					rows[i-1].before(row);
				}
				else if(circular){
					jQuery(rows[rows.length -1]).after(row);
				}
			}
			else {
				if(i != (rows.length -1)) {
					jQuery(rows[i+1]).after(row);
				}
				else if(circular) {
					rows[0].before(row);
				}
			}
			return;
		}
	}
}


/********** HENTE FELTVERDIER *************/
function getFeltverdier(root, array, fieldClass) {
	var className = "value";
	if((fieldClass !== undefined) & (fieldClass !== "")) {
		className = fieldClass;
	}
	var inputs = root.find("input."+className);
	var ta = root.find("textarea."+className);
	var selects = root.find("select."+className);
	var hidden = root.find("div."+className);
	var i = 0;
	for(i = 0; i < inputs.length; i++) {
		inputs[i] = jQuery(inputs[i]);
		if((inputs[i].parents(".del").length === 0) && (inputs[i].parents(".lastIncluded").length === 0) && (inputs[i].parents(".inkludertSkalIkkeMed").length === 0)) {
			if(inputs[i].attr("type") == "radio") {
				if(inputs[i].is(":checked")) {
					array[inputs[i].attr("name")] = inputs[i].val();
				}
			}
			else if(inputs[i].attr("type") == "checkbox") {
				if(inputs[i].is(":checked")) {
					array[inputs[i].attr("id")] = inputs[i].val();
				}
				else {
					/*array[inputs[i].attr("id")] = 0;	*/
				}
			}
			else {
				array[inputs[i].attr("id")] = inputs[i].val(); 
			}
		}
	}
	for(i = 0; i < ta.length; i++) {
		ta[i] = jQuery(ta[i]);
		if((ta[i].parents(".del").length === 0) && (ta[i].parents(".lastIncluded").length === 0) && (ta[i].parents(".inkludertSkalIkkeMed").length === 0)) {
			array[ta[i].attr("id")] = ta[i].val(); 
		}
	}
	for(i = 0; i < selects.length; i++) {
		selects[i] = jQuery(selects[i]);
		if((selects[i].parents(".del").length === 0) && (selects[i].parents(".lastIncluded").length === 0) && (selects[i].parents(".inkludertSkalIkkeMed").length === 0)) {
			array[selects[i].attr("id")] = selects[i].val(); 
		}
	}
	for(i = 0; i < hidden.length; i++) {
		hidden[i] = jQuery(hidden[i]);
		if((hidden[i].parents(".del").length === 0) && (hidden[i].parents(".lastIncluded").length === 0) && (hidden[i].parents(".inkludertSkalIkkeMed").length === 0)) {
			array[hidden[i].attr("id")] = hidden[i].html(); 
		}
	}
	var deleted = root.find("div.del");
	for(i = 0; i < deleted.length; i++) {
		deleted[i] = jQuery(deleted[i]);
		if (deleted[i].parents(".lastIncluded").length === 0) {
			array["del_" + deleted[i].attr("id").substring(2)] = "1";
		}
	}
}
/******** END HENTE FELTVERDIER ***********/
/************* DIALOG **************/
function Display_Popup(xmlDoc, id, klasse, top, left) {
	var popupElement = $("#dialogPopUpWrapper");
	if(popupElement.length) {
		popupElement.remove();
	}
	popupElement = $("<div></div>");
	popupElement.attr("id", "dialogPopUpWrapper").addClass("dialogPopUpWrapper");
	if(klasse) {popupElement.addClass(klasse);}
	
	if(id !== undefined) {
		var parent = $("#"+id).parent();
		popupElement.insertAfter($("#"+id));
		parent.removeClass("hidden");
	}
	else {
		popupElement.insertAfter($("#disableBG"));	
	}	
	 $("#disableBG").removeClass("hidden");
	
	if((top !== undefined) && (left !== undefined)) {
		popupElement.css("top", top).css("left", left);
	}
	else { 
		myCenter(popupElement, popupElement.width(), popupElement.height());
	}
}
function Display_Inline_Popup(xmlDoc, method, id, outsideClass) {
	var newElem = $("<div></div>");
	newElem.attr("id", "dialogPopUpWrapperInline");
	var parent;
	var outside = false;
	if(outsideClass) {
		outside = $("#"+id).parents("."+outsideClass).length > 0;
	}
	var fromList = false;
	if(!outside) {
		parent = $("#"+id).parent();
		newElem.insertAfter($("#"+id));
		parent.removeClass("hidden");
		fromList = true;
	}
	else {
		parent = $("."+outsideClass);
		newElem.insertAfter(parent);
	}
	redrawElements(xmlDoc, "", "", "");
	
	var pos = newElem.position();
	newElem.css("top",  pos.top);
	newElem.css("left", pos.left);
	var width = newElem.width();
	var height = newElem.height();
	var newElemDummy = $("<div></div>");
	newElemDummy.css("width", width + "px").css("height", (height - 8) + "px").attr("id", "dialogPopUpWrapperInlineDummy").addClass("dialogPopUpWrapperDummy");
	newElem.addClass("dialogPopUpWrapper");
	if((fromList) || (graBak)) {
		newElemDummy.insertAfter(newElem);
	}
	else {
		parent.append(newElemDummy);
	}	
	
	bg = $("#disableBG");
	bg.removeClass("hidden");
	
	ScrollPosition.setX($(window).scrollLeft());
	ScrollPosition.setY($(window).scrollTop());
}

function dialogCancel(source, method, sendParameters) {
	var dialogID = "dialogPopUpWrapper";
	if(sendParameters) {
		var root = jQuery($("."+dialogID)[0]);
		getFeltverdier(root, AJAX.parameters);
	}
	$("."+dialogID+"Dummy").remove();
	$("."+dialogID).remove();
	$("#disableBG").addClass("hidden");
		
	$(window).scrollTop(ScrollPosition.getY());
	$(window).scrollLeft(ScrollPosition.getX());
	ScrollPosition.clear();
	
	if(method) {
		AJAX.url = "/4daction/" + method;
		AJAX.post();
	}	
}

function dialogValidate(source, method) {
	var root = jQuery($(".dialogPopUpWrapper")[0]);
	getFeltverdier(root, AJAX.parameters);
	AJAX.url = "/4daction/" + method;
	AJAX.post();
}
/************ END DIALOG ***********/


function inkludertSkalMed(e) {
	var delElement = jQuery(eventTrigger(e));
	if(delElement.is(":checked")) {
		delElement.parent().removeClass("inkludertSkalIkkeMed").addClass("inkludertSkalMed");
		delElement.parent().find(".inkludertDetaljer").removeClass("hidden");
	}
	else {
		delElement.parent().removeClass("inkludertSkalMed").addClass("inkludertSkalIkkeMed");
		delElement.parent().find(".inkludertDetaljer").addClass("hidden");
	}
}

function moveRowDown(e) {
	if(!customMoveRowDown(e)) {
		//Lage en generisk metode.
	}
}

/***** SLETTE RADER I INKLUDERTE LISTER ****/
function deleteRow(e) {
	var delElement = jQuery(eventTrigger(e));
	var row = $("#r_" + delElement.attr("id").substring(2));
	if(row) {
		row.addClass("del");
		delElement.removeClass("delPict").addClass("unDelPict").unbind("click").click(unDeleteRow);
		//Kjøre custom funksjon
		customRowDeleted(delElement.attr("id"), true);
	}
	else {
		displayError("[delrows] Finner ikke element med id: " + delElement.attr("id").substring(2));
	}
}

function unDeleteRow(e) {
	var delElement = jQuery(eventTrigger(e));
	var row = $("#r_" + delElement.attr("id").substring(2));
	if(row) {
		row.removeClass("del");
		delElement.removeClass("unDelPict").addClass("delPict").unbind("click").click(deleteRow);
		//Kjøre custom funksjon
		customRowDeleted(delElement.attr("id"), false);
	}
	else {
		displayError("[delrows] Finner ikke element med id: " + delElement.attr("id").substring(2));
	}
}
/***END SLETTE RADER I INKLUDERTE LISTER *****/

function axReplaceRecordId(id) {
	var pos = id.lastIndexOf("_") + 1;
	return id.substring(0, pos) + (parseInt(id.substring(pos), 10) - 1);
}

function axReplaceFieldId(id, recordId) {
	var pos = id.lastIndexOf("_");
	if(!recordId) {recordId = axReplaceRecordId(id.substring(0, pos - 1));}
	if(id.charAt(0)=='d') {return "d_" + recordId;}
	if(isNaN(parseInt(id.charAt(0), 10))) {return id.substring(0, 2) + recordId + id.substring(pos);}
	else {return recordId + id.substring(pos);}
}

function axSetValueById(id, value) {
	setValueById(id, value);
	setValueById("v" + id.substring(1), value);
}


function validateInput(value, id) {
	var newValue, oldValue;
	newValue = value;
	var oldElement = $("#h" + id.substring(1));
	if(oldElement) {
		oldValue = oldElement.textContent;
		if(newValue != oldValue) {
			return customValidateInput(id, oldValue, newValue);
		}
		else {
			return ["1", newValue];
		}
	}
	else {
		return ["1", newValue];
	}
}

function showNextRow(id) {
	var element = $("#"+id);
	var row = element.parents(".row");
	var nextRow = $("#"+axReplaceRecordId(row.attr("id")));
	if(nextRow.length >0) {
		if(nextRow.hasClass("hidden")) {
			nextRow.removeClass("hidden").addClass("firstIncluded");
			row.removeClass("firstIncluded");
		}
		$("#"+axReplaceFieldId(id, nextRow.attr("id").substring(2))).focus();
	}
	
}

function isRowDeleted (id) {
	var row = getRecordElement(id);
	if(row) {
		var theId = row.id;
		theId = "d" + theId.substring(1);
		var del = $(theId);
		if(del) {
			if(del.hasClass("undelPict")) {
				return true;
			}
		}
	}
	return false;
}
function notValidated(notValid, type, doAlert) {
	for(idName in notValid) {
		if(idName != "length") {
			label = idName; //(type=="output"? getColumnLabel(idName): getLabel(idName));
			if(label) {
				if(doAlert) {
					alert(label + " er obligatorisk.");
					input = $((type=="output"? "o": "i") + label.substring(1));
					if(input) {setTimeout(input.focus(), 100);}
				}
				break;
			}
		}
	}
}

function emptyRow(root) {
	var inputs = root.find("input.felt");
	var i;
	for(i = 0; i < inputs.length; i ++ ) {
		inputs[i] = jQuery(inputs[i]);
		if(inputs[i].attr("type") == "checkbox") {
			if(inputs[i].is(":checked")) {
				return false;
			}
		}
		else {
			if(inputs[i].val().length > 0) {
				return false;
			}
		}
	}
	var options = root.find("option");
	for(i = 0; i < options.length; i ++ ) {
		options[i] = jQuery(options[i]);
		if((options[i].is("checked")) && (options[i].val() !== 0)  && (options[i].val() !== "")) {
			return false;
		}
	}
	return true;
}

/************** KALL TIL SERVER *********************/
function call4D(e) {
	var source = jQuery(eventTrigger(e));
	setAJAXAction(source);
	AJAX.parameters.id = source.attr("id");
	getFeltverdier($(document), AJAX.parameters, AJAX.parameters.id);
	AJAX.post();
}
function menu4D(e) {
	var i = 0;
	var source = jQuery(eventTrigger(e));
	var menu;
	if(source.hasClass("menu4D")) {
		menu = source;
		var menues = jQuery(menu.parents(".menuBar4D")[0]).find(".menuItems4D");
		for(i = 0; i < menues.length; i++) {
			menues[i] = jQuery(menues[i]);
			if(menues[i].attr("id") !== menu.attr("id")+"Items") {
				menues[i].addClass("hidden");
			}
		}
		$("#"+source.attr("id")+"Items").toggleClass("hidden");
	}
	else {
		AJAX.parameters.item4D = source.attr("id");	
		menu = $("#"+source.parent().attr("id").replace("Items",""));
		AJAX.parameters.menu4D = menu.attr("id");
		setAJAXAction(menu);
		AJAX.post();
		jQuery(menu.parents(".menuBar4D")[0]).find(".menuItems4D").addClass("hidden");
	}
	
}
var tooltip = {
	delay : 600,
	timer : undefined	
};

function tip4D(e) {
	var source = jQuery(eventTrigger(e));
	setAJAXAction(source);
	AJAX.parameters.id = source.attr("id");
	tooltip.timer = setTimeout("AJAX.post()", tooltip.delay);
}
function tip4DHide(e) {
	clearTimeout(tooltip.timer);
	var source = jQuery(eventTrigger(e));
	var id = "tip_"+source.attr("id");
	$("#"+id).fadeOut("slow", function () {$("#"+id).remove();});
}

function myTooltip(id, txt) {
	var source = $("#"+id);
	var tipElement = $("<div></div>");
	tipElement;
	$(".tooltip").remove();
	var pos = source.position();
	tipElement.html(txt).attr("id", "tip_"+id).addClass("tooltip").css("top", pos.top + 20).css("left", pos.left + 20).insertAfter(source).fadeIn("slow");
}

function deleteRecord(file, record, confirmation) {
	if(confirmation) {
		var advarsel = customDeleteText(file, record);
		if(confirm(advarsel)) {
			AJAX.url = "/4daction/WA_Delete_Record";
			AJAX.parameters.file = file;
			AJAX.parameters.record = record;
			AJAX.post();
		}
	}
	else {
		AJAX.url = "/4daction/WA_Delete_Record";
		AJAX.parameters.file = file;
		AJAX.parameters.record = record;
		AJAX.post();
	}
}

function saveRecord(table, record, rootID) {
	AJAX.url = "/4daction/WA_Save_Record";
	getFeltverdier($("#"+rootID), AJAX.parameters);
	AJAX.parameters.table = table;
	AJAX.parameters.record = record;
	AJAX.post();
}

/*************END KALL TIL SERVER *******************/

/****************** COOKIES ***********************/
function cookiesEnabled(name) {
	if(!name) {name = "test_";}
	$.cookie(name, "1");
	if($.cookie(name) == "1") {
		$.cookie(name, null);
		return true;
	}
	return false;	
}
/******************* END COOKIES **********************/

/****************** WINDOW ***************************/
function windowResized() {
	var popup = $("#popup");
	if(popup.length>0) {
		if(popup.css("display") == "block") {
			myCenter(popup, popup.width(), popup.height());
		}
	}
}

function windowOnScroll(e) {
	var popup = $("#popup");
	if(popup.length>0) {
		if(popup.css("display") == "block") {
			myCenter(popup, popup.width(), popup.height());
		}
	}
}
/********************* END WINDOW *************************/
function setAJAXAction(obj) {
	var classes = [];
	classes = obj.attr("class").split(" ");
	if(classes.length < 2) {}
	else {
		AJAX.url = "/4daction/"+classes[1];	
	}
}


/* isNewRow skal returnere true dersom 1ste (mandatory) felt ikke er fylt ut */
function isNewRow (id) {
	var row = getRecordElement(id);
	var mandatory = row.find(".mandatory");
	if(mandatory.length > 0) {
		return mandatory.html() === "";
	}
	return false;
}

function setValue(id, value) {
	var element = $("#"+id);
	if(element.length > 0) {
		var formatedValue;
		var className = element.attr("class");
		if(!element.hasClass("input")) {
			format = element.attr("format");
			if(format) {
				if(format.length > 0) {
					formatedValue = theString(value, format);
				}
			}
			setValueById(id, formatedValue? formatedValue: value);
		}
	}
}

function WindowOnLoad() {
	customWindowOnLoad();
}

function toggleDiv(e) {
	var source = jQuery(eventTrigger(e));
	var id = source.attr("id");
	var target = $("#"+id+"_");
	if(target.hasClass("hidden")) {
		if((target.html() !== "") && (source.hasClass("fillAction"))) {
			target.removeClass("hidden");
		}
		else if(!source.hasClass("noAction")) {
			setAJAXAction(source);
			AJAX.parameters.Item = id;
			if(target.html()!=="") {
				AJAX.parameters.hasHTML = 1;
			}
			AJAX.post();
		}
		else {
			target.removeClass("hidden");
			source.removeClass("collapsed").addClass("expanded");	
		}
		source.removeClass("itemShow").addClass("itemHide");
	}
	else {
		target.addClass("hidden");
		source.removeClass("itemHide").addClass("itemShow");
	}
}


function hideShowById(source, hideClass, showClass, oclickAction) {
	oclickAction();
	source = jQuery(source);
	var className = source.attr("id");
	var animator = source.children(":first");
	var hide = animator.hasClass(hideClass);
	if(hide) {
		$("."+className).addClass("hidden");
	}
	else {
		$("."+className).removeClass("hidden");
	}
	if(hide){animator.attr("class", showClass);}
	else {animator.attr("class", hideClass);}
}
function myExpand(source, classname, method) {
	var knapp = $("#"+source.id + "Btn");
	var knappTxt = $("#"+source.id + "Hide");
	if(knapp.hasClass("PilHoyreSvart_btn")) {
			AJAX.url = "/4daction/"+method;
			AJAX.parameters.id = source.id;
			AJAX.post();
	}
	else {
		$("#"+source.id+"Content").html("");
		knapp.removeClass("PilNedSvart_btn").addClass("PilHoyreSvart_btn");
		if(knappTxt) {
			knappTxt.addClass("hidden");
			$("#"+source.id + "Show").removeClass("hidden");
		}
	}
}
function myCollapse(source, classname, customonclick, expandaction, hideclass) {
	customonclick();
	source = jQuery(source);
	source.find(".ikon_liten").attr("class", "ikon_liten ikon_Expand left");
	source.unbind("click").click(expandaction);
	collapse = $(document).find("."+classname);
	if(hideclass === undefined) {
		$(document).find("."+classname).addClass("hidden");
	}
	else {
		$(document).find("."+classname).addClass(hideclass);
	}
}

function My_Convert_Date(dato) {
	if(dato.length == 10) {
		return dato.substring(6)+"/"+dato.substring(3,5)+"/"+dato.substring(0,2);
	}
	else {
		return dato;
	}
}


/***************** AJAX RESPONS ***************************/
function ajaxResponse(response) {
	var redirect = $(response).find("redirect").text();
	var refresh = $(response).find("Refresh").text();
	var error = $(response).find("error").text();
	var tooltip = $(response).find("tip");
	if(redirect) {
		getRedirect(redirect);
	}
	 if (refresh == "1") {
		window.location.reload();
	}
	else if (error.length>0) {
		alert(error);
	}
	else if (tooltip.length) {
		myTooltip(AJAX.parameters.id, jQuery(tooltip).text());
	}
	else {
		var popup = $(response).find("#dialogPopUpWrapper");
		if(popup.length) {
		 	Display_Popup(response);
		}
		
		AJAX.clearParameters = true;
		AJAX.clearExtra = true;
		var method = AJAX.url.replace("/4daction/","");
		var pos = method.indexOf("/");
		if(pos > -1) {
			method = method.substring(0, pos);
		}
		switch (method) {
			case "WA_SOMETHING":
				var id = AJAX.parameters.Item;
				redrawElements(response);
				$("#"+id).removeClass("itemShow").addClass("itemHide");
				break;
			default:
				customAjaxResponse(response, method);
		}
		
		//Må sentrere popup her da den først har fått innhold nå.
		if(popup.length) {
			myCenter($("#dialogPopUpWrapper"));
		}
		
		var records;var top;
	}
	//Setter parametere blank igjen.
	if(AJAX.clearExtra) {AJAX.extra = {};}
	if(AJAX.clearParameters) {AJAX.parameters = {};AJAX.url = "";}
}

function setActions(root) {
	if(!root) {root = $(document);}
	
	//setter action for å slette inkludert rad
	jQuery(root.find(".deleteIncluded")).unbind("click").click(deleteRow);
	
	//setter action for å flytte inkludert rad ned
	jQuery(root.find(".moveRowDown")).unbind("click").click(moveRowDown);
	
	//setter action på sjekkboks om record i inkludert liste skal med
	jQuery(root.find(".skalmed")).unbind("click").click(inkludertSkalMed);
	
	jQuery(root.find(".call4D")).unbind("click").click(call4D);
	jQuery(root.find(".item4D")).unbind("click").click(toggleDiv);
	jQuery(root.find(".menu4D")).unbind("click").click(menu4D);
	jQuery(root.find(".menuItems4D > .menuItem4D")).unbind("click").click(menu4D);	
	jQuery(root.find(".tip4D")).unbind("mouseover").unbind("mouseout").mouseover(tip4D).mouseout(tip4DHide);
	jQuery(root.find(".preview4D")).unbind("mouseover").unbind("mouseout").mouseover(showPreview).mouseout(hidePreview);

	
	//actions spesielt for dette programmet
	customSetActions(root);
}

var RelateHelper = {
	relateTarget : {},
	relateId : [],
	relateData : [],
	relateText : [],
	setRelateArrays : function (id , data) {
		var i = jQuery.inArray(id, this.relateId);
		if(i==-1) {
			i = jQuery.inArray("", this.relateId);
			if(i==-1) {i = this.relateId.length;}
		}
		this.relateId[i] = id;
		this.relateData[i] = data;
	},
	validateRelate : function (id) {
		var OK = false;
		var source = $("#"+id);
		var i = jQuery.inArray(id, this.relateId);
		var soft = source.hasClass("softRelation");
		if((i>-1) && (!soft)){
			var data_ = this.relateData[i];
			if(data_) {
				if(data_ == source.val()) {
					OK = true;	
				}
			}
		}
		if((soft) && (source.val() !==""))  {
			if(source.hasClass("add")) {
				showNextRow(id);
			} 
			OK = true;
		}
		else {
			if(!OK){ 
				if(source.parents(".included").length > 0) {
					row = $("#r"+id.substring(1, id.lastIndexOf("_")));
					if(!row.hasClass("firstIncluded")) {
						row.addClass("hidden");
					}
				}
				axSetValueById(id, "");
			}
			else if(source.hasClass("add")) {
				showNextRow(id);		
			}
		}
		customOnValidatedRelated(id);
		return OK;
	}
};

function setRelateActions() {
	var relatingFields = $(document).find("input.relate");
	if(relatingFields.length > 0 ) {
		setRelateAction(relatingFields, "", "", false);
	}
	
	var customRelatingFields = $(document).find("input.customRelate");
	if(customRelatingFields.length > 0 ) {
		var action = "/4daction/web_find_records?ASP="+gup("ASP")+"&amp;Random="+gup("Random")+"&amp;ax=1";
		setRelateAction(customRelatingFields, action, "", false);
	}
}

function setRelateAction(aElements, action, options, skipBlur) {
	var j = 0;
	if(!action) {action = "/4daction/web_find_related_records_?ASP="+gup("ASP")+"&amp;Random="+gup("Random")+"&amp;ax=1";}
	if(!options) {options = {noCache: true, delay: 200, minLength:1 };}
	if(!options.noCache) {options.noCache = true;}
	if(!options.deferRequestBy) {options.delay = 200;}
	if(!options.minChars) {options.minLength = 1;}
	setOnSelect = options.onSelect === undefined;
	for(j = 0; j < aElements.length; j++) {
		aElements[j] = jQuery(aElements[j]);
		
		//Setter verdiene i hjelperen for at feltet ikke skal blankes ut hvis man bare er innom uten å endre tekst...
		if(aElements[j].val()!=="") {
			RelateHelper.setRelateArrays(aElements[j].attr("id"), aElements[j].val());
		}
		options.source = function(req, add) {
			//setter relateTarget
			RelateHelper.relateTarget = jQuery(this.element[0]);
			
			//pass request to server
			req.id = this.element[0].id;
			req.name = this.element[0].name;
			customRelateParameters(req);
			$.getJSON(action, req,  function(data){
				add(data.source);
				if(!skipBlur) {
					//RelateHelper.setRelateArrays(req.id, data.source);
				}
			});
		};
		options.select = function(e, ui){  
			var hidden = ui.item.hidden;
			var label = ui.item.label;
			var target = RelateHelper.relateTarget;
			var hiddenTarget = $("#v_"+target.attr("id").substring(2));
			if(hiddenTarget[0].tagName == "INPUT") {
				hiddenTarget.val(hidden); //Setter hidden-verdi	
			}
			else {
				hiddenTarget.html(hidden); //Setter hidden-verdi
			}
			RelateHelper.setRelateArrays(target.attr("id"), label);
		};
		aElements[j].autocomplete(options);
		if(!skipBlur) {
			aElements[j].blur(function() {
				source =  jQuery(this).attr("id");
				setTimeout("RelateHelper.validateRelate('"+source+"')", 200);
			});
		}
	}
}

/************** DATEPICKER ***************/
jQuery(function($){
	$.datepicker.regional['no'] = {
		closeText: 'Lukk',
		prevText: '&laquo;Forrige',
		nextText: 'Neste&raquo;',
		currentText: 'I dag',
		monthNames: ['Januar','Februar','Mars','April','Mai','Juni','Juli','August','September','Oktober','November','Desember'],
		monthNamesShort: ['Jan','Feb','Mar','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Des'],
		dayNamesShort: ['Søn','Man','Tir','Ons','Tor','Fre','Lør'],
		dayNames: ['Søndag','Mandag','Tirsdag','Onsdag','Torsdag','Fredag','Lørdag'],
		dayNamesMin: ['Sø','Ma','Ti','On','To','Fr','Lø'],
		weekHeader: 'Uke',
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	$.datepicker.setDefaults($.datepicker.regional['no']);
});
/************* END DATEPICKER **************/

/********** CALLBACKS ************/
function redrawElements(xmlDoc) {
	var i = 0;
	var message = $(xmlDoc).find("message").text();
	//Lukke dialog
	if (message == "dialogCancel") {
		dialogCancel("");
		message = "";
	}
	if(message !== "") {
		alert(message);
	}
	
	//slette elementer
	var remove = xmlDoc.getElementsByTagName("delete");
	for(i = 0; i < remove.length; i++) {
		$("#"+remove[i].getAttribute("id")).remove();
	}
		
	//skjule elementer
	var hide = xmlDoc.getElementsByTagName("hide");
	for(i = 0; i < hide.length; i++) {
		$("#"+hide[i].getAttribute("id")).addClass("hidden");
	}	
	//vise elementer
	var show = xmlDoc.getElementsByTagName("show");
	for(i = 0; i < show.length; i++) {
		$("#"+show[i].getAttribute("id")).removeClass("hidden");
	}
	
	//fjerne klasser
	var removeClass = xmlDoc.getElementsByTagName("removeClass");
	for(i=0;i< removeClass.length;i++) {
		$("#"+removeClass[i].getAttribute("id")).removeClass(removeClass[i].textContent?removeClass[i].textContent:removeClass[i].childNodes[0].nodeValue);
	}
	//sette klasser
	setClass = xmlDoc.getElementsByTagName("setClass");
	for(i=0;i<setClass.length;i++) {
		$("#"+setClass[i].getAttribute("id")).addClass(setClass[i].textContent?setClass[i].textContent:setClass[i].childNodes[0].nodeValue);
	}
	
	//sette verdier i input-felter/textare
	setValues = xmlDoc.getElementsByTagName("setValue");
	for(i=0;i<setValues.length;i++) {
		$("#"+setValues[i].getAttribute("target")).val(setValues[i].textContent?setValues[i].textContent:setValues[i].childNodes[0].nodeValue);
	}

	//Fylle ut områder og sette actions på ny html
	replaceArea = xmlDoc.getElementsByTagName("replace");
	var htmlReplaceId, htmlReplaceElement, content, highlight, width, height;
	for(i = 0; i < replaceArea.length; i++) {
		htmlReplaceId = jQuery(replaceArea[i]).attr("id");
		htmlReplaceElement = $("#"+htmlReplaceId);
		if(htmlReplaceElement.length) {
			htmlReplaceElement.removeClass("hidden");
			content = replaceArea[i].getElementsByTagName("content")[0].textContent;
			if(!content) {
				content = replaceArea[i].getElementsByTagName("content")[0].text;
			}
			if(content) {
				htmlReplaceElement.html(content);
			}			
			inputs_ = htmlReplaceElement.find(".relate");
			setRelateAction(inputs_);
			inputs_ = htmlReplaceElement.find(".customRelate");
			var action = "/4daction/web_find_records?ASP="+gup("ASP")+"&amp;Random="+gup("Random")+"&amp;ax=1";
			setRelateAction(inputs_, action);
			setActions(htmlReplaceElement);	
		}
		else {
			alert("[redrawElements: replaceArea] Fant ikke html-elementet med id: " + htmlReplaceId);
		}
	}
		
	//Legge til i område
	addToArea = $(xmlDoc).find("add");
	var htmlAddToId, htmlAddToElement;
	for( i = 0; i < addToArea.length; i++) {
		htmlAddToId = jQuery(addToArea[i]).attr("id");
		htmlAddToElement = $("#"+htmlAddToId);
		if(htmlAddToElement.length) {
			content = addToArea[i].getElementsByTagName("content")[0].textContent;
			if(!content) {
				content = addToArea[i].getElementsByTagName("content")[0].text;
			}
			if(content) {
				var tagName = htmlAddToElement[0].nodeName.toLowerCase();
				switch (tagName) {
					case "table":
						if(htmlAddToElement.lastChild) {
							if(htmlAddToElement.lastChild.tagName.toLowerCase() == "tbody") {
								htmlAddToElement.lastChild.append(content);
								break;
							}
						}
						htmlAddToElement.append(content);
						break;
					default:
						htmlAddToElement.append(content);
						break;
				}
			}
			inputs_ = htmlAddToElement.find(".relate");
			setRelateAction(inputs_);
			setActions(htmlAddToElement);
		}
		else {
			alert("[redrawElements: addToArea] Fant ikke html-elementet med id: " + htmlAddToId);
		}	
	}
	
	//custom action
	customRedrawElements(xmlDoc);	
}
/********************** END AJAX RESPONS ***********************/