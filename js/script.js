/* 	==|== scripts.js =====================================================
	Author: James Borillo
	Company: Lex Norsk Samfunnsinformasjon as
	Dept: IT department
	========================================================================== */

$.noConflict();

/*********************************************
	Do immediately even DOM is not loaded yet
**********************************************/

jQuery(function($){ 		
	/******** Priority onLoad events ******/
	$('.intervjuToggle, .spesialutgaver').css('display', 'block');
	$('.kursGraf .kursList li:not(.selected, .selectable)').hide();
	$('.kursGraf .bedriftliste li:not(.selected, .selectable, .skille)').hide();
	$('.kursIntervjuer li:not(.selected) a').hide();
	$('.kursIntervjuer li.select a').show();
	/******** Other onLoad events **********/
	$('.fancy_menu div').removeClass('hover_class'); // HACK: hover_class is implemented for the sake of this functionality only in order to disable css hover to implement jQuery effect
	$('.map_canvas_button').css('display', 'inline');
	$('ul').prev('p').css('margin-bottom','0');//removes margin for <p> if <ul> is next sibling	
});

/***************************
	End of Do immediately
****************************/


jQuery(document).ready(function($){

	/*
		DO the ff. when DOM is Loaded.
	*/
		
		url_hash = window.location.hash;
		
		if (url_hash.length > 0) {
			if (url_hash == '#openskole') {
				$('#skole_nav .fancy_menu_parent ul').slideDown(500);
				$('.my_overlay').css({'width': '100%', 'height': $('body').height()});
			} else { // If hash has a parent .toggleContainer, then open the container.
				$(url_hash).parents('.toggleContainer').addClass('toggleExpanded');
			}
		}
		
		$('.toggleContainer').each(function(){ // Collapse all toggleContainer except if it has a class=toggleExpanded	
			if ( ! $(this).hasClass('toggleExpanded') ) {
				$(this).addClass('toggleCollapsed');
				$('.toggleText', this).css({'display': 'none'});
			} 
		});		
				
		/*
			iPad landscape and below: 
		*/
		if ($(window).width() <= 1024) { //show year for every course
			$('.kursList li:not(.spesialutgaver)').each(function(){
				var duration = $(this).attr('data-yr');
				/** Activate this for media query
				$('a', this).append(' (' + duration + ' år)');
				**/
			});
		}

		/*
			Parent p selector for ol and ul
			@process Add margin-bottom: 0;
		*/
		$('.artikkeltekst ul, .artikkeltekst ol').prev('p').css('margin-bottom','0');
	
	/*
		EVENTS
		User interaction like (click, hover, mouseover, mouseout etc) can be coded inside
		this wrapper ready document method
	*/
		
		/*
			Show submenus 
		*/
		$('#fylke_nav .fancy_menu_parent, #skole_nav .fancy_menu_parent, #prog_nav .fancy_menu_parent, #skoleaar_nav .fancy_menu_parent').click(function(event){ 
			if ( $('ul', this).is(':hidden') ) {
				$('ul', this).slideDown(500);
				$('.my_overlay').css({'width': '100%', 'height': $('body').height()});
			} else {
				$('.my_overlay').css({'width': '0%', 'height': '0%'});
				$('ul', this).slideUp(300);
			}
		});
		
		$('#falang_nav .fancy_menu_parent').click(function(event){ 
			if ( $('ul', this).is(':hidden') ) {
				if ( $('body').hasClass('forsiden') ) {
					$('ul', this).css('bottom', 30); // Hack to make it slideUp.
				}
				$('ul', this).slideDown(500);
				$('.my_overlay').css({'width': '100%', 'height': $('body').height()});
			} else {
				$('.my_overlay').css({'width': '0%', 'height': '0%'});
				$('ul', this).slideUp(300);
			}
		});
		
		/*
			Pop-out Norway's map
		*/
		$('.showFylkesKart').fancybox({
			type: 'ajax',
			beforeLoad : function(){
				preloadImages();
				flyttkart('LenkerTilKart');
			},
			helpers	: {
				overlay : {
					opacity	: '0.3',
					css		: { 'background' : '#DDD'}
				}
			},
			beforeShow: function(){
				if ($(this).attr('href').search('#') > 0) {
			        $.get('/4daction/WS_Vilblino_STR/16201/106/'+$.getUrlVars()['Lan'], function(data) {
						$('#LenkerTilKart h3').html('<div style="padding-left:20px;background:url('+web_static_content_path+'/img/arrow_down_red_l.png)no-repeat 0 2px;color:red;font-weight:bold;min-height:20px">'+data+'</div>');
					});
				}
			},
			afterShow: function(){
				var href_val = $(this).attr('href'); //href from link (ajax)
				
				var hash_pos = href_val.search('#');
				if (hash_pos > 0) {
					var href_hash = href_val.substring(hash_pos);
				} else {
					var href_hash = '';
				}
				var allVars = $.getUrlVars(); // URL from the browser
				var side_param = getParameterByName('Side', href_val);
				var parsedURL = '';
				var the_hash = '';
				var param_count = 0;
				if (allVars.length > 0) {
                       for (var i = 0; i < allVars.length; i++) {
						var assoc = allVars[i];
						var separator = '';
						if (param_count == 0) separator = "?"; else separator = "&";
						if (assoc != "Fylke"){
							if ( (assoc == "Side") && (side_param != "") ){
								param = side_param;
							} else if ( (assoc == "Side") && (allVars[assoc]=="1.5.1")) { // Bedriftstilbud pr kurs. Skal tilbake til graf ved bytte av fylke
								param = "1.5"
							} else {
								param = allVars[assoc];
							}
							var hash_pos_browser = param.indexOf("#");
							if(hash_pos_browser>-1) {
								param = param.substring(0, hash_pos_browser);
							}
							parsedURL = parsedURL + separator + assoc + "=" + param; 
							param_count += 1;
						}
						
					}
				} else {
					parsedURL = '?Artikkel='; // Hack :(
				}
				
				if (side_param=="1.2.1") { // Slått ut skoler på fagsiden uten at fylke er valgt
					var fag = "V."+$(this.element).attr('data-fag');
					parsedURL = replaceQueryParameter(	parsedURL, "Fag", fag);
					parsedURL = parsedURL+"&Expand="+href_hash.replace("#expand_","");
					
				}
							
				$('#LenkerTilKart a, map area').each(function(){
					var fylke_nr = $(this).attr('data-fylke');
					var newURL = parsedURL + '&Fylke=' + fylke_nr + href_hash;	
					$(this).attr('href', newURL);
				});
				

			}
		});
		
		/********* Opplæringskontor ****************/
		$('.kursGraf .medlemmerToggle').click(function(){
			var indeks = $(this).attr('data-kurs-indeks');
			
			var subheaders="";
			
			var myElement = '#kurskommuner_' + indeks + ' .skoleliste li.kommuneToggle';
			if($(myElement).length) {
				var subheaders='#kurskommuner_' + indeks + ' .skoleliste li.kommuneToggle_';
			}
			else {
				var myElement = '#kurskommuner_' + indeks + ' .skoleliste li:not(.selectable)';
			}
			if ($(myElement).is(":visible")) {
				$(myElement).slideUp(500);
				$(this).removeClass('expand');
			
				var myElement = '#kurskommuner_' + indeks + ' .skoleliste li:not(.selectable)';
				$(myElement).slideUp(500);
				$('#kurskommuner_' + indeks + ' .skoleliste li.kommuneToggle').removeClass('expand');
				
				if(subheaders.length>0) {
					$(subheaders).slideUp(500);
				}	
								
			} else {
				$(myElement).slideDown(500, function(){
					//$('.kursKolonne').css('min-height', $(this).parent().parent().height()); // increase min-height for other columns
				});
				$(this).addClass('expand');
				if(subheaders.length>0) {
					$(subheaders).slideDown(500);
				}	
				
					//$('#kursKolonne' + colnum + ' .spesialutgaver').addClass('expand');	
			}
		});
		
		$('.kursGraf .kommuneToggle').click(function(){
	    	var kommune = $(this).attr('data-kommune');
	    	var myElement = ':not(".selected").kurskommunerbedrifter_' + kommune; // only elements without class .selected
	    	if ($(myElement).is(":visible")) {
	    		$(myElement).slideUp(500);
	    		$(this).removeClass('expand');
	    	} else {
	    		$(myElement).slideDown(500);
	    		$(this).addClass('expand');
	    	}
	    });
		
		
		/************** kursGraf ***********************/
		$('.kursGraf .grafToggle').click(function(){
			var colnum = $(this).attr('data-colnum');
			var myElement = '#kursKolonne' + colnum + ' .kursList li:not(.selectable, .spesialutgave)';
			if ($(myElement).is(":visible")) {
				$(myElement).slideUp(500);
				$(this).removeClass('expand');
				var container_height = $(this).parent().parent().height();
			} else {
				$(myElement).slideDown(500, function(){
					$('.kursKolonne').css('min-height', $(this).parent().parent().height()); // increase min-height for other columns
				});
				$(this).addClass('expand');	
				//$('#kursKolonne' + colnum + ' .spesialutgaver').addClass('expand');	
			}
		});
    
    	$('.kursGraf .spesialutgaver').click(function(){
	    	var colnum = $(this).attr('data-spesialutgave');
	    	var myElement = ':not(".selected").spesialutgave' + colnum; // only elements without class .selected
	    	if ($(myElement).is(":visible")) {
	    		$(myElement).slideUp(500);
	    		$(this).removeClass('expand');
	    	} else {
	    		$(myElement).slideDown(500);
	    		$(this).addClass('expand');
	    	}
	    });
        
    	$('.kursGraf .intervjuToggle').click(function(){
	    	var listnum = $(this).attr('data-listnum');
	    	var myElement = '#kursIntervjuer' + listnum + ' li:not(.selected) a';
	    	if ($(myElement).is(":visible")) {
	    		$(myElement).slideUp(500);
	    		$(this).removeClass('expand');
	    	} else {
	    		$(myElement).slideDown(500);
	    		$(this).addClass('expand');
	    	}
	    });  
    
    	/************ .artikkel ************/
	    $('.toggleContainer .toggleTitle').click(function(){  	
			
			var my_parent_element = $(this).parent();
			if ( my_parent_element.hasClass('toggleCollapsed') ) {
				my_parent_element.removeClass('toggleCollapsed');
			} else {
				my_parent_element.addClass('toggleCollapsed');
				my_parent_element.removeClass('toggleExpanded'); // in case it exist
			}
			
			$('~ .toggleText', this).slideToggle('fast');

	    });
	
		$('a.img_gallery').fancybox(); // Galeries are created from elements who have the same "rel" attribute so that multiple galeries can be implemented in a single page.
		$('.various').fancybox({ // use this selector for generic overlay
			maxWidth	: 900,
			maxHeight	: 900,
			fitToView	: false,
			width		: '70%',
			height		: '70%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
		
		/************ My overlay **************/
		$('.my_overlay').click(function() {
			$('.my_overlay').css({'width': '0%', 'height': '0%'});
			$('.fancy_menu ul').slideUp(300);
		});
			
		/********** Mobile devices **********/
		$('.showVertMenu').toggle(
			function(){
				$('#vertical_nav').animate({'left': '-0%', 'z-index': '2000'}, 500);
				$('.my_overlay').css({'width': '100%', 'height': $('body').height()});
				$('.showVertMenu').html('Skjul meny');
			},
			function(){
				$('#vertical_nav').animate({'left': '-60%'}, 500);
				$('.my_overlay').css({'width': '0%', 'height': '0%'});
				$('.showVertMenu').html('Vis meny');
			}
		);
		$('#vertical_nav a:not(".dir")').click(function(){ // USER INTERACTION: show user that menu hides again after clicking a link from the vertical menu
			$('#vertical_nav').animate({'left': '-60%'}, 500);
		});
		
	/*************************************
		END OF EVENTS
	**************************************/
	
		/***********************************
		DO the ff. when page is fully loaded  
		Insert codes that you want to execute inside this wrapper right after the page has loaded everything 
		(Everything == assets == js, css, images, APIs etc)
		***********************************/
			$(window).load(function(){ 
			
				$('.medlemmerToggle').css('display', 'block'); // Show only when page is fully loaded to hide elements repositioning
				

			
				
				$('.grafToggle').css('display', 'block'); // Show only when page is fully loaded to hide elements repositioning
				$('.kursKolonne').each(function(i){ // Hide grafToggle if it only have 1 element from the list
					var size = $('#kursKolonne' + (i + 1) + ' .kursList li:not(".selected, .spesialutgaver")').size(); // except selected and spesialutgaver
					if (size == 0) {
						$('#kursKolonne' + (i + 1) + ' .grafToggle').remove(); 
					}
				});
				//loop through spesialutgaver if exist
				$('.spesialutgaver').each(function(j){
					var this_nr = $(this).attr('data-spesialutgave');
					var children_count = $(':not(".selected").spesialutgave' + this_nr).size();
					if (children_count == 0) {
						$(this).remove();
					}
				});
					
				var wrapper_height = $('#wrapper').height();
				var viewport_height = $(window).height();
				
				var max_height = 0;
				$('.kursKolonne .kursList li').each(function(i){
					if ($(this).hasClass('selected')){ // transfer the selected list to the top list
						var this_height = $(this).parent().height();
						if ( max_height < this_height) {
							max_height = this_height;
						}
						$(this).prependTo($(this).parent());
					}
					var kursKolonne_duration = $(this).parent().parent().attr('data-yr');
					var li_duration = $(this).attr('data-yr');
					if ( li_duration > kursKolonne_duration ) {
						var element_width = $(this).width();
						$(this).width(element_width+10);
						$('a', this).append(' (' + li_duration + '-&aring;rig)');
					}
				});
				$('.kursKolonne .kursList').css('min-height', max_height); //increase height for shorter columns
				
				$('.kursKolonne .kursIntervjuer li').each(function(i){
					if ($(this).hasClass('selected')){ // transfer the selected list to the top list
						$(this).prependTo($(this).parent());
					}
				});
				
				if ($(window).width() > 1024) {
					
					$('#content_100, #content_75').css('min-height', wrapper_height - 150);
					var footer_offset = $('.footer').offset().top;
					var doc_height = $(document).height();
					$('.footer').height(doc_height - footer_offset); // Extend the footer to the doc bottom if body is short
					var kursgraf_height = $('.kursGraf').height();
					$('.kursGraf .kursKolonne').css({'min-height': kursgraf_height});
                                        
                    var container_width = $('#container').width()
                    $('.header_content').css('width', container_width);

                    /* Random images of students */
                    var img_count = 10;
                    var curr_num = Math.floor(Math.random() * (img_count) );
                    var image = web_static_content_path +'/data/random_img/students/'+curr_num+'.png';		                            
					$('.random_students').css({
						'position': 'fixed',
						'height': '1000px',
						'width': '1000px',
						'top': '45px',
						'background': 'url("'+image+'") -30px top no-repeat',
                   	    'margin-left': container_width,
                   	    'opacity': 1
					});
                    /* End of Random images of students */
					
				} else if ($(window).width() <= 1024) { // Hack for iPad to fit vertical_nav if it is higher than the content
				
					var vertical_nav_height = $('#vertical_nav').height();
					var max_height = Math.max(wrapper_height, vertical_nav_height);
					$('#content_100, #content_75').css('min-height', max_height - 150);
					
				}
				
				$('.fancy_menu ul').css('max-height',  viewport_height - 250);
				
				if (typeof jcarousel_img_obj != 'undefined') {
					$('.jcarousel_small ul').empty();//empty preloaded content
					$.each(jcarousel_img_obj,function(i,item){
						$('.jcarousel_small ul').append('<li><img src="'+item.file+'"></li>');
					});
					$(':not(".no_Carousel").prog_media_img').append('<div class="btn_container"><span class="btn_prev"></span><span class="btn_nxt"></span></div>');
					$('.jcarousel_small').jCarouselLite({
						btnNext:'.btn_nxt',
						btnPrev:'.btn_prev',
						visible: 1,
						mouseWheel: true,
						auto: 10000
					});
				}
					
				if (typeof my_utd_videos != 'undefined') {
					
					for (var i = 0; i < my_utd_videos.length; i++) {
		
						if ( $.browser.device ) {
							var class_pop_video = 'no_pop_video';
							var anchor_pop_video = web_static_content_path +'/data/video/'+my_utd_videos[i].kode+'/'+my_utd_videos[i].vid_dir+'/Utdanningsprogram.mp4';
						} else {
							var class_pop_video = 'pop_video_'+i+'" data-count="'+i;
							var anchor_pop_video = '#pop_video_'+my_utd_videos[i].kode+'_'+my_utd_videos[i].vid_dir;
						}

						$('#video_container_'+my_utd_videos[i].kode+'_'+my_utd_videos[i].vid_dir).html('<a class="'+class_pop_video+'" href="'+anchor_pop_video+'"><img class="video_img" src="'+ web_static_content_path +'/data/video/'+my_utd_videos[i].kode+'/'+my_utd_videos[i].vid_dir+'/Utdanningsprogram_thumb.png" alt=""></a>');

						
						$("a.pop_video_"+i).fancybox({
							'closeClick': false,
							'scrolling': 'no',
							'width': 600,
							'height': 500,
							'afterLoad': function() {
								var vid_count = $(this.element).attr('data-count');								
								var videoSetup = {
									'flashplayer': web_static_content_path +'/jwplayer/mediaplayer-5.8-licensed/player.swf',
									'id': 'playerID',
									'width': '786',
									'height': '432',
									'stretching': 'exactfit',
									'image': web_static_content_path +'/data/video/'+ my_utd_videos[vid_count].kode +'/'+my_utd_videos[vid_count].vid_dir+'/Utdanningsprogram.png',
									'logo.file': web_static_content_path +'/img/logo.png',
									'logo.hide': false,
									'logo.position': 'top-right'
								};
								videoSetup.levels = my_utd_videos[vid_count].levels; // append a specific object from my_utd_videos
									
								jwplayer('mediaplayer_'+ my_utd_videos[vid_count].kode +'_'+my_utd_videos[vid_count].vid_dir).setup(videoSetup);
							}
						});	
						
					}
				}
					
			});
			
		/*****************************
		End of DO when page is fully loaded
		******************************/
	
}); // End of Document ready

/*****************************
	Functions
******************************/

/** This method is subject to deletion - jCarousel is used instead **/
function rotateImages(selector, imgs_dir, img_count, trans_interval) { 
	var curr_num = 1;
	setInterval(function(){
		jQuery(selector).animate({opacity: 0}, 2000, function(){
 			if (curr_num >= (img_count)) {
 				curr_num = 0;
 			}
 			jQuery(this).html('<img src="' + imgs_dir + (++curr_num) +'.jpg">');
 				
 		}).animate({opacity: 1}, 1500);
	}, trans_interval);
}

/*****************************
	End of Functions
******************************/