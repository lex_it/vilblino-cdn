<?php
	/***
		Author: James Borillo
		Company: Lex Norsk Samfunnsinformasjon as
		Description: This will generate a marker with number on.
	***/

	$string = $_GET['text'];
	$marker_img = $_GET['marker'];
	
	$font = 'arial';
	
	//unfortunately we still must do some offsetting
	switch (ord(substr($string,0,1))) {
	 case 49: //1
		$offset = -2;
		break;
	 case 55: //7
		$offset = -1;
		break;
	 case 65: //A
		$offset = 1;
		break;
	 case 74: //J
		$offset = -1;
		break;
	 case 84: //T
		$offset = 1;
		break;
	 case 99: //c
		$offset = -1;
		break;
	 case 106: //j
		$offset = 1;
		break;
	}
	if (strlen($string) == 1) {
	$fontsize = 10.5;
	} else if (strlen($string) == 2) {
	$fontsize = 9;
	} else {
	$fontsize = 8;
	$offset = 0; //reset offset
	}
	
	$bbox = imagettfbbox($fontsize, 0, $font, $string);
	$width = $bbox[2] - $bbox[0] + 1;
	$height = $bbox[1] - $bbox[7] + 1;
	
	$image_name = "http://cdn.vilblino.applex.no/gmarker/" . $marker_img . ".png"; // be sure that this image is in the directory

	$im = imagecreatefrompng($image_name);
	imageAlphaBlending($im, true);
	imageSaveAlpha($im, true);
	//$black = imagecolorallocate($im, 0, 0, 0);
	
	imagettftext($im, $fontsize, 0, 11 - $width/2 + $offset, 9 + $height/2, $black, $font, $string);
	
	header("Content-type: image/png");
	imagepng($im);
	imagedestroy($im);
?>